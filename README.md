# YAGPDB-regex-censor


## What is this?

This is a script for Yet Another General Purpose Discord Bot (YAGPDB or YAG) that can work with Censor Bot to block text based on a regex string.
Whenever a user (admin or not) sends a message containing a blocked string the bot will delete the message (if it isn't handled by Censor Bot) and send a DM to the user containing their original message with the blocked word(s) underlined.

This was originally made for my own Discord server to prevent swearing.


## How do I use it?

1. In your server's control panel on YAG's website go to: Core -> Custom Commands
2. Create a new custom command
3. Change the trigger type to regex and make the trigger ".\*" (so it will trigger every message)
4. Paste the contents of discord_bot_censor_code_merge.cs into the response
5. Set the roles/channels
6. Save
7. (optional) In your discord create a channel "bot-log" which YAG can send messages to.
	Whenever YAG blocks something that isn't handled by Censor Bot it will log the message here.

If you want to test that it is working or see examples of how acronyms are blocked take a look at the test file.
You can paste that whole message into discord if you want to test that the script is working.
